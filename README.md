## finalfour

[![build status](https://gitlab.com/johnjvester/finalfour/badges/master/build.svg)](https://gitlab.com/johnjvester/finalfour/commits/master)

> The finalfour project is a sample application created to utilize [RandomGenerator](https://gitlab.com/johnjvester/RandomGenerator) in 
> order to simulate games for the 2016 NCAA Men's Basketball tournament.
>
> Running the main() method will perform 100,000 iterations of the games played during the 2016 tournament.  The program will keep track of 
> the correct picks for each bracket, the final four teams and the tournament winner.  At the end, statistics are provided via usage of the 
> simple ```System.out.println()``` console output.

### Getting Started

In order to use this project, simply clone/download the project and Run the main() method.

Using RandomGenerator 1.4, the ratingLevel can be adjusted by making updates to the following line of code:

```List<Team> winner = randomGenerator.randomize(thisGame, 1, 1);```

Simply change the final element in the constructor, providing a value between 0 (zero) and 3 (three), which provides significantly different results.

#### Logging

There is a log4j2.xml file in this project, which is set to error-level logging.  It is possible to change the log levels, but doing so 
could consume significant disk space when performing 100,000 iterations (or more).

#### Additional Information

For additional information on RandomGenerator, please review the following JavaDoc:

[http://johnjvester.gitlab.io/RandomGenerator-JavaDoc/](http://johnjvester.gitlab.io/RandomGenerator-JavaDoc/)

Created by [John Vester](https://www.linkedin.com/in/johnjvester), because I truly enjoy writing code.