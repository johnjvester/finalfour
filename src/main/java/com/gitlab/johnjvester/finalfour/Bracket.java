package com.gitlab.johnjvester.finalfour;

import lombok.Data;

@Data
public class Bracket {

    public Bracket(String name) {
        this.name = name;
    }

    private String name;
}
