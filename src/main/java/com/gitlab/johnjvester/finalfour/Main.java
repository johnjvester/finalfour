package com.gitlab.johnjvester.finalfour;

import com.gitlab.johnjvester.randomizer.RandomGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {

    private static final String SOUTH = "South";
    private static final Integer SOUTH_KEY = 1;
    private static final String WEST = "West";
    private static final Integer WEST_KEY = 2;
    private static final String EAST = "East";
    private static final Integer EAST_KEY = 3;
    private static final String MIDWEST = "Midwest";
    private static final Integer MIDWEST_KEY = 4;

    public static void main(String[] args) {
        HashMap<Integer, ArrayList<Team>> teamMap = new HashMap<Integer, ArrayList<Team>>();

        loadTeams(teamMap);

        int correctWinner = 0;
        int allCorrect = 0;
        int southCorrect = 0;
        int westCorrect = 0;
        int eastCorrect = 0;
        int midwestCorrect = 0;

        Team realSouthWinner = teamMap.get(SOUTH_KEY).get(1);
        Team realWestWinner = teamMap.get(WEST_KEY).get(1);
        Team realEastWinner = teamMap.get(EAST_KEY).get(0);
        Team realMidwestWinner = teamMap.get(MIDWEST_KEY).get(9);

        int loopSize = 100000;
        for (int i = 0; i < loopSize; i++) {
            if (i % 100 == 0) {
                System.out.println("Processing " + i + " iterations ...");
            }

            boolean south = false;
            boolean west = false;
            boolean east = false;
            boolean midwest = false;

            Team southWinner = processListOfTeams(teamMap.get(SOUTH_KEY));

            if (realSouthWinner.getName().equals(southWinner.getName())) {
                south = true;
                southCorrect++;
            }

            Team westWinner = processListOfTeams(teamMap.get(WEST_KEY));

            if (realWestWinner.getName().equals(westWinner.getName())) {
                west = true;
                westCorrect++;
            }

            Team eastWinner = processListOfTeams(teamMap.get(EAST_KEY));

            if (realEastWinner.getName().equals(eastWinner.getName())) {
                east = true;
                eastCorrect++;
            }

            Team midwestWinner = processListOfTeams(teamMap.get(MIDWEST_KEY));

            if (realMidwestWinner.getName().equals(midwestWinner.getName())) {
                midwest = true;
                midwestCorrect++;
            }

            if (south && west && east && midwest) {
                allCorrect++;
            }

            ArrayList<Team> finalFour = new ArrayList<Team>();
            finalFour.add(southWinner);
            finalFour.add(eastWinner);
            finalFour.add(midwestWinner);
            finalFour.add(westWinner);

            Team winner = processListOfTeams(finalFour);

            if (winner.getName().equals(southWinner.getName())) {
                correctWinner++;
            }
        }

        System.out.println("Finished iterations ... processing results ...");

        float southPercent = (southCorrect * 100.0f) / loopSize;
        float westPercent = (westCorrect * 100.0f) / loopSize;
        float eastPercent = (eastCorrect * 100.0f) / loopSize;
        float midwestPercent = (midwestCorrect * 100.0f) / loopSize;
        float allPercent = (allCorrect * 100.0f) / loopSize;
        float winnerPercent = (correctWinner * 100.0f) / loopSize;

        System.out.println(
                "Number of times (out of " + loopSize + ") where South bracket winner (" + realSouthWinner.toString() + ") was correct " + southCorrect + " ("
                        + southPercent + "%)");
        System.out.println(
                "Number of times (out of " + loopSize + ") where West bracket winner (" + realWestWinner.toString() + ") was correct " + westCorrect + " ("
                        + westPercent + "%)");
        System.out.println(
                "Number of times (out of " + loopSize + ") where East bracket winner (" + realEastWinner.toString() + ") was correct " + eastCorrect + " ("
                        + eastPercent + "%)");
        System.out.println(
                "Number of times (out of " + loopSize + ") where Midwest bracket winner (" + realMidwestWinner.toString() + ") was correct " + midwestCorrect
                        + " (" + midwestPercent + "%)");
        System.out.println("Number of times (out of " + loopSize + ") where all bracket winners were correct " + allCorrect + " (" + allPercent + "%)");
        System.out.println(
                "Number of times (out of " + loopSize + ") where tournament winner (" + realSouthWinner.toString() + ") was correct " + correctWinner + " ("
                        + winnerPercent + "%)");
    }

    private static Team processListOfTeams(ArrayList<Team> teamList) {
        if (teamList.size() > 1) {
            ArrayList<Team> nextRound = processBracket(teamList);
            return processListOfTeams(nextRound);
        } else {
            return teamList.get(0);
        }
    }

    private static ArrayList<Team> processBracket(ArrayList<Team> thisRound) {
        ArrayList<Team> returnBracket = new ArrayList<Team>();

        int start = 0;
        int end = (thisRound.size() - 1);

        while (start < end) {
            Team winner = pickWinner(thisRound.get(start), thisRound.get(end));
            returnBracket.add(winner);

            start++;
            end--;
        }

        return returnBracket;
    }

    private static Team pickWinner(Team home, Team visitor) {
        RandomGenerator randomGenerator = new RandomGenerator();

        ArrayList<Team> thisGame = new ArrayList<Team>();
        thisGame.add(home);
        thisGame.add(visitor);

        List<Team> winner = randomGenerator.randomize(thisGame, 1, 1);

        //System.out.println("Winner of " + home.toString() + " vs " + visitor.toString() + " is " + winner.get(0).getName());

        return winner.get(0);
    }

    private static void loadTeams(HashMap<Integer, ArrayList<Team>> teamMap) {
        // Source = http://www.espn.com/mens-college-basketball/rpi/_/year/2016/sort/RPI

        Bracket bracket = new Bracket(SOUTH);
        ArrayList<Team> teamList = new ArrayList<Team>();

        Team team = new Team("Kansas", 668, 1, bracket);
        teamList.add(team);
        team = new Team("Villanova", 646, 2, bracket);
        teamList.add(team);
        team = new Team("Miami (FL)", 633, 3, bracket);
        teamList.add(team);
        team = new Team("California", 613, 4, bracket);
        teamList.add(team);
        team = new Team("Maryland", 615, 5, bracket);
        teamList.add(team);
        team = new Team("Arizona", 597, 6, bracket);
        teamList.add(team);
        team = new Team("Iowa", 591, 7, bracket);
        teamList.add(team);
        team = new Team("Colorado", 586, 8, bracket);
        teamList.add(team);
        team = new Team("UConn", 585, 9, bracket);
        teamList.add(team);
        team = new Team("Temple", 568, 10, bracket);
        teamList.add(team);
        team = new Team("Wichita State", 576, 11, bracket);
        teamList.add(team);
        team = new Team("South Dakota State", 589, 12, bracket);
        teamList.add(team);
        team = new Team("Hawaii", 551, 13, bracket);
        teamList.add(team);
        team = new Team("Buffalo", 631, 14, bracket);
        teamList.add(team);
        team = new Team("UNC Asheville", 518, 15, bracket);
        teamList.add(team);
        team = new Team("Austin Peay", 489, 16, bracket);
        teamList.add(team);

        teamMap.put(SOUTH_KEY, teamList);

        bracket = new Bracket(WEST);
        teamList = new ArrayList<Team>();

        team = new Team("Oregon", 656, 1, bracket);
        teamList.add(team);
        team = new Team("Oklahoma", 637, 2, bracket);
        teamList.add(team);
        team = new Team("Texas A&M", 607, 3, bracket);
        teamList.add(team);
        team = new Team("Duke", 606, 4, bracket);
        teamList.add(team);
        team = new Team("Baylor", 597, 5, bracket);
        teamList.add(team);
        team = new Team("Texas", 595, 6, bracket);
        teamList.add(team);
        team = new Team("Oregon State", 590, 7, bracket);
        teamList.add(team);
        team = new Team("Saint Joe's", 606, 8, bracket);
        teamList.add(team);
        team = new Team("Cincinnati", 578, 9, bracket);
        teamList.add(team);
        team = new Team("VCU", 584, 10, bracket);
        teamList.add(team);
        team = new Team("N. Iowa", 560, 11, bracket);
        teamList.add(team);
        team = new Team("Yale", 579, 12, bracket);
        teamList.add(team);
        team = new Team("UNC-Wilmington", 577, 13, bracket);
        teamList.add(team);
        team = new Team("Green Bay", 528, 14, bracket);
        teamList.add(team);
        team = new Team("CSU Bakersfield", 526, 15, bracket);
        teamList.add(team);
        team = new Team("Holy Cross", 455, 16, bracket);
        teamList.add(team);

        teamMap.put(WEST_KEY, teamList);

        bracket = new Bracket(EAST);
        teamList = new ArrayList<Team>();

        team = new Team("N. Carolina", 642, 1, bracket);
        teamList.add(team);
        team = new Team("Xavier", 634, 2, bracket);
        teamList.add(team);
        team = new Team("West Virginia", 626, 3, bracket);
        teamList.add(team);
        team = new Team("Kentucky", 627, 4, bracket);
        teamList.add(team);
        team = new Team("Indiana", 599, 5, bracket);
        teamList.add(team);
        team = new Team("Notre Dame", 587, 6, bracket);
        teamList.add(team);
        team = new Team("Wisconsin", 581, 7, bracket);
        teamList.add(team);
        team = new Team("USC", 577, 8, bracket);
        teamList.add(team);
        team = new Team("Providence", 583, 9, bracket);
        teamList.add(team);
        team = new Team("Pittsburgh", 575, 10, bracket);
        teamList.add(team);
        team = new Team("Michigan", 571, 11, bracket);
        teamList.add(team);
        team = new Team("Chattanooga", 574, 12, bracket);
        teamList.add(team);
        team = new Team("Stony Brook", 567, 13, bracket);
        teamList.add(team);
        team = new Team("Stephen F. Austin", 564, 14, bracket);
        teamList.add(team);
        team = new Team("Weber State", 534, 15, bracket);
        teamList.add(team);
        team = new Team("Florida Gulf Coast", 474, 16, bracket);
        teamList.add(team);

        teamMap.put(EAST_KEY, teamList);

        bracket = new Bracket(MIDWEST);
        teamList = new ArrayList<Team>();

        team = new Team("Virginia", 653, 1, bracket);
        teamList.add(team);
        team = new Team("Michigan State", 627, 2, bracket);
        teamList.add(team);
        team = new Team("Utah", 633, 3, bracket);
        teamList.add(team);
        team = new Team("Iowa State", 603, 4, bracket);
        teamList.add(team);
        team = new Team("Purdue", 613, 5, bracket);
        teamList.add(team);
        team = new Team("Seton Hall", 603, 6, bracket);
        teamList.add(team);
        team = new Team("Dayton", 605, 7, bracket);
        teamList.add(team);
        team = new Team("Texas Tech", 586, 8, bracket);
        teamList.add(team);
        team = new Team("Butler", 573, 9, bracket);
        teamList.add(team);
        team = new Team("Syracuse", 564, 10, bracket);
        teamList.add(team);
        team = new Team("Gonzaga", 577, 11, bracket);
        teamList.add(team);
        team = new Team("Little Rock", 580, 12, bracket);
        teamList.add(team);
        team = new Team("Iona", 550, 13, bracket);
        teamList.add(team);
        team = new Team("Fresno State", 564, 14, bracket);
        teamList.add(team);
        team = new Team("Middle Tennessee", 556, 15, bracket);
        teamList.add(team);
        team = new Team("Hampton", 505, 16, bracket);
        teamList.add(team);

        teamMap.put(MIDWEST_KEY, teamList);
    }
}
