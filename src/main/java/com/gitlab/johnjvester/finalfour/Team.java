package com.gitlab.johnjvester.finalfour;

import lombok.Data;

@Data
public class Team {
    public Team(String name, Integer rating, Integer seed, Bracket bracket) {
        this.name = name;
        this.rating = rating;
        this.seed = seed;
        this.bracket = bracket;
    }

    @Override
    public String toString() {
        return seed + ". " + name + " (" + rating + ")";
    }

    private String name;
    private Integer rating;
    private Integer seed;
    private Bracket bracket;
}
